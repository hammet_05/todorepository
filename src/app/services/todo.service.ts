import { Injectable } from '@angular/core';
import{List} from '../Models/list.model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root'
})
export class TodoService
 {
  list:List[]=[];


  constructor() 
  {
   this.chargeStorage();
   
   }
  
   public createList(title:string)
   {
      const newList=new List(title);
      this.list.push(newList);
      this.saveStorage();
      return newList.id;

   }

   getList(id:string | number)
   {
      id=Number(id);
      return this.list.find( listData=>listData.id===id);
   }
   
   public saveStorage()
   {
     localStorage.setItem('data',JSON.stringify(this.list));
   }

   public chargeStorage()
   {
     if(localStorage.getItem('data'))
     {
      this.list=JSON.parse(localStorage.getItem(('data')));
     }
     
   }
}
