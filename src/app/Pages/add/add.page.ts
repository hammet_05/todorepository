import { Component, OnInit } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';
import { ActivatedRoute } from '@angular/router';
import { List } from 'src/app/Models/list.model';
import { TaskItem } from 'src/app/Models/taskItem.model';


@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  list:List;
  nameItem='';
  constructor(private todoService:TodoService,private route:ActivatedRoute)
   {

      const listId=this.route.snapshot.paramMap.get('listId');
      this.list=this.todoService.getList(listId);
   }

  ngOnInit() {
  }
  addItem()
  {
     if(this.nameItem.length===0)
     {
          return;
     }
     const newListItem=new TaskItem(this.nameItem);
     this.list.taskItem.push(newListItem);
     console.log(newListItem);
     console.log(this.list);
     this.nameItem='';
     this.todoService.saveStorage();
     
  }
  checkChange(item:TaskItem)
  {
    console.log({item});
   
    const listPendings=this.list.taskItem
                            .filter(dataItem=>!item.completed)
                            .length;
    if(listPendings===0)
    {
        this.list.finishedAt=new Date();
        this.list.completed=true;
    }            
    else
    {
      this.list.finishedAt=null;
      this.list.completed=false;
    }                
    this.todoService.saveStorage();

    console.log(this.todoService.list);
  }
  delete(index:number)  
  {
    this.list.taskItem.splice(index,1);
    this.todoService.saveStorage();
  }
}
