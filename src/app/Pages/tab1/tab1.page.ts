import { Component } from '@angular/core';
import {TodoService} from '../../services/todo.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { List } from 'src/app/Models/list.model';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page 
{

  constructor(public todoServices:TodoService,
              private router:Router, 
              private alertController:AlertController) 
  {
     // this.todoServices.getLists();
  }

async addTasktoList()
  {
    //this.router.navigateByUrl('/add');
   // this.router.navigateByUrl('/add');
    const alert=await this.alertController.create({
      header:'New List',
      inputs:
        [
          {
            name:'title',
            type:'text',
            placeholder:'Name of the list',
            handler:()=>{
              console.log('CAncela');
            }
          },
         
        ],
        buttons:
        [
          {
            text:'Cancel',
            role:'cancel'
          },
          {
            text:'Create',
            handler:(data)=>{
              console.log(data);
              if(data.title.length===0)
              { 
                return;
              }
              const listId=this.todoServices.createList(data.title);
              this.router.navigateByUrl(`/tabs/tab1/add/${listId}`);

            }

          }
        ]

    })
    alert.present();

  }

  selectedList(list:List)
  {
    console.log({list});
    
    //this.router.navigateByUrl(`/tabs/tab1/add/${list.id}`);
  }

}
