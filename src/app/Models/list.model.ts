import { TaskItem } from './taskItem.model';



export class List
{
    public id:number;
    public title:string;
    public createAt:Date;
    public finishedAt: Date;
    public completed:boolean;
    public taskItem:TaskItem[]; 

    constructor(_title:string)
    {
        this.title=_title;
        this.createAt=new Date;
        this.completed=false;
        this.taskItem=[];
        this.id=new Date().getTime();
    }
}