


export class TaskItem
{
    public descr : string;
    public completed: boolean;

    constructor(_descr:string)
    {
        this.descr=_descr;
        this.completed=false;
    }
}